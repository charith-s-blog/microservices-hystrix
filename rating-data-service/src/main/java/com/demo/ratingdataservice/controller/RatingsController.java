package com.demo.ratingdataservice.controller;

import com.demo.ratingdataservice.models.Rating;
import com.demo.ratingdataservice.models.UserRating;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/ratingsdata")
public class RatingsController {

    @RequestMapping("/users/{userId}")
    public UserRating getRating (@PathVariable("userId") String userId){
        List<Rating> ratings = Arrays.asList(
            new Rating("603", 10),
            new Rating("604", 10)
        );

        UserRating userRating = new UserRating();
        userRating.setUserRating(ratings);
        return userRating;
    }
}
